import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Today from "../views/Today.vue"

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Today',
    name: 'Today',
    component: Today
  },

]

const router = new VueRouter({
  mode:"history",
  routes
})

export default router